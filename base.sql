create database caisse;
<<<<<<< Updated upstream

create table Categorie(
    idCategorie int not null AUTO_INCREMENT primary key,
    nomCategorie varchar(150)
=======
create table categorie(
    
>>>>>>> Stashed changes
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table Produit(
    code varchar(3) not null primary key,
    idCategorie int not null,
    designation varchar(150),
    prixunit int not null,
    qttstock int not null,
    foreign key (idCategorie) references categorie(idCategorie) /*on delete cascade*/
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table Caisse(
    idCaisse int not null AUTO_INCREMENT primary key
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table Achat(
    idAchat int not null AUTO_INCREMENT primary key,
    dateHeure datetime,
    idCaisse int not null,
    foreign key (idCaisse) references Caisse(idCaisse)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table DetailAchat(
    idDetailAchat int not null AUTO_INCREMENT primary key,
    idAchat int not null,
    code varchar(3) not null,
    qtt int not null,
    foreign key (code) references Produit(code),
    foreign key (idAchat) references Achat(idAchat)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

create table Admin(
    idAdmin int not null AUTO_INCREMENT primary key,
    pseudo varchar(50),
    mdp varchar(255)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into categorie values(null, "Boisson");
insert into categorie values(null, "Boucherie");
insert into categorie values(null, "Fruit de mer");

insert into produit values("001", "1", "Fanta", 2000, 250);
insert into produit values("002", "1", "Heineken", 5000, 150);
insert into produit values("003", "1", "Natur eau", 2500, 150);
insert into produit values("004", "1", "Seven up", 7000, 200);
insert into produit values("005", "1", "Sprite", 4000, 200);

insert into produit values("006", "2", "Jambon", 6000, 1000);
insert into produit values("007", "2", "Poulet", 12000, 1000);
insert into produit values("008", "2", "Viande", 18000, 1000);
insert into produit values("009", "2", "Saucisse", 10000, 1000);
insert into produit values("010", "2", "Saucisson", 8000, 1000);

insert into produit values("011", "3", "Calmar", 22000, 1000);
insert into produit values("012", "3", "Crevette", 18000, 1000);
insert into produit values("013", "3", "Langouste", 44000, 1000);
insert into produit values("014", "3", "Rouget", 48000, 1000);
insert into produit values("015", "3", "Thon", 20000, 1000);

insert into caisse values("caisse1");
insert into caisse values("caisse2");
insert into caisse values("caisse3");


insert into admin values(null, "admin", sha1("1234"));