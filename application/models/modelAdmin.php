<?php
    if( ! defined('BASEPATH')) exit('No direct script access allowed');
    class ModelAdmin extends CI_Model
    {
        public function getAchatSem(){
            $sql="SELECT achat.dateHeure,week(achat.dateHeure) as semaine,sum(detailAchat.qtt*produit.prixunit) as recette from achat 
            join detailAchat on achat.idAchat=detailAchat.idAchat join produit on detailAchat.code=produit.code
            group by semaine";
            $query = $this->db->query($sql);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function getStatProd(){
            $sql="SELECT detailachat.code as code,produit.designation as nom,sum(detailachat.qtt) as somme from detailachat
            join produit on detailachat.code=produit.code
            group by detailachat.code order by sum(detailachat.qtt)";
            $query = $this->db->query($sql);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function getCaissier(){
            $query = $this->db->query('select * from admin');
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }
        
        public function suppProd($codeProd){
            $req = "delete from produit where code=%s";
            $req = sprintf($req,$codeProd);
             $this->db->query($req);
        }

        public function getProdByCode($code){
            $req="select * from produit where code=%s";
            $req=sprintf($req, $code);
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function modifier($code,  $prix, $qtt){
            $req = "update produit set prixunit=%s, qttstock=%s where code=%s ";
            $req = sprintf($req,$prix, $qtt, $code);
             $this->db->query($req);
        }

        public function getIdCatByNom($nom){
            $req = "select * from categorie where nomCategorie='%s'";
            $req = sprintf($req, $nom);
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function insertProd($code, $idCat, $des, $prix, $quant)
        {
            $req = "insert into produit values('%s', '%s', '%s', %s, %s)";
            $req = sprintf($req,$code, $idCat, $des, $prix, $quant);
             $this->db->query($req);

        }

        public function recherche($mot){
            $req="select * from produit where designation like '$mot%' or designation like '%$mot%' or designation like '%$mot'";
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function getCategorie(){
            $req="select * from categorie";
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function supprCat($cat){
            $req = "delete from categorie where idCategorie=%s";
            $req = sprintf($req,$cat);
             $this->db->query($req);
        }

        public function getCatById($id){
            $req="select * from categorie where idCategorie=%s";
            $req=sprintf($req, $id);
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
          
        }

        public function modifierCat($id, $nom){
            $req = "update categorie set nomCategorie='%s' where idCategorie=%s ";
            $req = sprintf($req,$nom, $id);
             $this->db->query($req);
        }

        public function insertCat($id, $nom){
            $req = "insert into categorie values('%s', '%s')";
            $req = sprintf($req,$id, $nom);
             $this->db->query($req);
        }

        public function rechercheCat($mot){
            $req="select * from categorie where nomCategorie like '$mot%' or nomCategorie like '%$mot%' or nomCategorie like '%$mot'";
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function statGlobal(){
            $req= "select count(*) from achat group by date ";
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function statProduit(){
            $req = "select count(*) from detailachat dt join achat a on dt.idAchat=a.idAchat group by a.dateHeure";
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function statCaisse(){
            $req="select count(*) frpm caisse join achat on caisse.idCaisse = achat.idCaisse group by achat.dateHeure";
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }

        public function top3Produit($date, $date2){
            $req= "select code, count(*) as chrono from detailachat group by code order by ordre desc limit 3 where dateHeure>='%s' and dateHeure<='%s'";
            $req= sprintf($req, $date, $date2);
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }
}
?>