<?php
    if( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Model1 extends CI_Model
    {
        
        public function get_info()
        {
            // On simule l'envoi d'une requête
            return array('auteur' => 'Chuck Norris',
                        'date' => '24/07/05',
                        'email' => 'email@ndd.fr');
        }
        public function get_caisse(){
            $query = $this->db->query('select * from caisse');
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }
        public function get_prod(){
            $query = $this->db->query('select * from produit');
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }
        public function get_prodByCate($id){
            $query = $this->db->query('select * from produit where idCategorie='.$id);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }
        public function get_cate(){
            $query = $this->db->query('select * from categorie');
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }
        /*public function get_achat_bycaisse($id){
            $req = "select * from achat where idCaisse=%s";
            $req = sprintf($req,$id);
            $query = $this->db->query($req);
            $list = array();
            foreach ($query->result_array() as $row){
                $list[] = $row;
            }
            return $list;
        }
        public function get_prod_achat($liachat){
            $list = array();
            for($i=0;$i<count($liachat);$i++){
                $req = "select * from produit where code=%s";
                $req = sprintf($req,$this->db->escape($liachat[$i]['code']));
                $query = $this->db->query($req);
                $list[] = $query->row_array();
            }
            return $list;
        }*/
        public function get_prod_bycode($nom){
            $query = $this->db->query("select * from produit where code='".$nom."'");
            $prod = $query->row_array();
            return $prod;
        }
        public function insertachat($idCaisse){
            $this->db->query("insert into achat values(null,now(),".$idCaisse.")");
        }
        public function insertdetachat($lachat,$idachat){
            $qtstr = strval($lachat['qtt']);
            $code = $lachat['code'];
            $req = "insert into detailAchat values(null,%s,'%s',%s)";
            $req = sprintf($req, $idachat, $code, $qtstr);
            //echo $req;
            $this->db->query($req);
        }
        public function getlastachat(){
            $query = $this->db->query("select max(idAchat) as lastId from achat");
            $prod = $query->row_array();
            return $prod;
        }
        /*public function updateprod($code,$isa){
            $this->db->query("update produit set qttstock=".$isa." where code='".$code."'");
        }*/
    }
?>