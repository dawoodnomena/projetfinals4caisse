<?php
    $list=$lstatProd;

    $data1 = '';
    $data2 = '';
    
    for($i=0;$i<count($list);$i++) {
        $data1 = $data1 . "'". $list[$i]['nom']."',";
        $data2 = $data2 . '"'. $list[$i]['somme'].'",';
	}

    $data1 = trim($data1,",");
    $data2 = trim($data2,",");
   
?>

<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Statistique achat par semaine</title>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
</head>
<body>

<div class="col-sm-6 mb-3 mb-sm-0">
<table style="border-collapse : separate;border-spacing : 75px;width:50px">
    <tr>
        <th><h1>Code Produit</h1></th>
        <th><h1>Nom  </h1></th>
        <th><h1>Nombre</h1></th>
    </tr>
    <?php for($i=0;$i<count($list);$i++){ ?>
    <tr>
        <td><?php echo $list[$i]['code']; ?></td>
        <td><?php echo $list[$i]['nom']; ?></td>
        <td><?php echo $list[$i]['somme']; ?></td>
    </tr>
    <?php } ?>
</table>
</div>

<canvas id="myChart"></canvas>
        
        <script>
            var ctx = document.getElementById('myChart').getContext('2d');
            var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'bar',

            // The data for our dataset
            data: {
                labels: [<?php echo $data1; ?>],
                datasets: [{
                    label: 'top 3',
                    data: ['<?php echo $data2; ?>],
                    barPercentage: 1,
                    barThickness: 6,
                    maxBarThickness: 8,
                    minBarLength: 2,
                    backgroundColor:["rgba(255, 99, 132, 0.6)","rgba(255, 159, 64, 0.4)","rgba(255, 205, 86, 0.4)"],
                    borderColor:["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)"],
                    borderWidth:1,
                   
                }]    
            },

            // Configuration options go here
            options: {
                scales: {
                    xAxes: [{
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            }
            }
            }); 
        </script>
        </div>
    <body>    
</html>


