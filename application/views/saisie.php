
<style type="text/css">
input {
  text-align:center;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  margin: 0;
}
</style>

<h1>Saisie</h1>
<h4>caisse n° <?php echo $caisse; ?></h4>
<p> Nom Categorie :
<select>
        <option onclick="getProd('')"> Tous les produits</option>
        <?php for ($i=0;$i<count($licate);$i++){ ?>
                <option onclick="getProd(<?php echo $licate[$i]['idCategorie']; ?>)"><?php echo $licate[$i]['nomCategorie']; ?></option>
        <?php } ?>
</select>
</p>
<h4>Liste Produit</h4>
<div class="row">
      <div class="col-lg-4">
      
<div id="resultat">
<div class="card bg-light offset-1 col-md-10 offset-1 " >
                <div class="card-body" style="background-color: #fff">
<table>
        <?php for ($i=0;$i<count($liprod);$i++){ ?>
      <tr>
      <td><?php echo $liprod[$i]['designation'] ?></td>
      <form action=<?php echo base_url('',null).'welcome/achat' ?> method="post">
      <input name="code" type="hidden" value="<?php echo $liprod[$i]['code']; ?>" />
      <td><!--<input style="width: 55px;" name="qtt" type="number"/>-->

<div>
  <button style="width: 55px;" type="button" onclick="this.parentNode.querySelector('[type=number]').stepDown();">-</button>

  <input style="width: 65px;" type="number" name="qtt" min="0" max="100" value="0">

  <button style="width: 55px;" type="button" onclick="this.parentNode.querySelector('[type=number]').stepUp();">+</button>
</div>
      </td>
      <td><input type="submit" value="Ajouter"/></td>
      </form>
      </tr>
    <?php } ?>
</table>
</div>
</div>
</div>

</div>
      <div class="col-lg-8">
<?php if(isset($listAchat)){ ?>

    <div class="card bg-light offset-1 col-md-10 offset-1 " >
                <div class="card-header"><h4>Liste des achats</h4>
                <div class="card-body" style="background-color: #fff">
        <table width="400" class="table table-borderless">
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Quantite totale</th>
            </tr>
            <?php for($i=0;$i<count($listAchat);$i++){ ?>
                <tr>
                    <td><img style="width : 100px; height : 100px" src="<?php echo images_url($listAchat[$i]['code'].'.jpg'); ?>"/></td>
                    <td><?php echo $listAchat[$i]['code']; ?></td>
                    <td><?php echo $listAchat[$i]['qtt']; ?></td>
                </tr>
            <?php } ?>
        </table>
            </div>
            </div>


<?php } ?>
<form action="<?php echo base_url('',null).'welcome/validerAchat' ?>" method="post">
<input type="submit" value="Valider Achat"/>
</form>
</div>
</div>
