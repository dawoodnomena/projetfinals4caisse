
<?php

$list=$lachatSem;

$data1 = '';
$data2 = '';

for($i=0;$i<count($list);$i++) {

    $data1 = $data1 . '"'. $list[$i]['recette'].'",';
    $data2 = $data2 . '"'. $list[$i]['semaine'].'",';
}
$data1 = trim($data1,",");
$data2 = trim($data2,",");
?>

<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Statistique achat par semaine</title>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
</head>
<body>
    <div class="col-sm-6 mb-3 mb-sm-0">
    <table style="border-collapse : separate;border-spacing : 75px;width:50px">
        <tr>
            <th><h1>Semaine</h1></th>
            <th><h1>Date  </h1></th>
            <th><h1>Recette</h1></th>
        </tr>
        <?php for($i=0;$i<count($list);$i++){ ?>
        <tr>
            <td><?php echo $list[$i]['semaine']; ?></td>
            <td><?php echo $list[$i]['dateHeure']; ?></td>
            <td><?php echo $list[$i]['recette']; ?> € </td>
        </tr>
        <?php } ?>
    </table>
    </div>
    
    <div>
    <canvas id="myChart"></canvas>
    
    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: [<?php echo $data2; ?>],
            datasets: [{
                label: 'Stat de ventes par semaine',
                borderColor: 'rgb(255, 99, 132)',
                data: [<?php echo $data1; ?>]
            }]
        },

        // Configuration options go here
        options: {}
        }); 
    </script>
    </div>
<body>    
</html>