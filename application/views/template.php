<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>  
<!doctype html>
<html lang="en">

<head>
   
    <!--====== Required meta tags ======-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>Caisse</title>

    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="<?php echo css_url("bootstrap.min.css") ?>">
    
    <!--====== Animate css ======-->
    <link rel="stylesheet" href="<?php echo css_url("animate.css") ?>">
    
    <!--====== Magnific Popup css ======-->
    <link rel="stylesheet" href="<?php echo css_url("magnific-popup.css") ?>">
    
    <!--====== Slick css ======-->
    <link rel="stylesheet" href="<?php echo css_url("slick.css") ?>">
    
    <!--====== Line Icons css ======-->
    <link rel="stylesheet" href="<?php echo css_url("LineIcons.css") ?>">
    
    <!--====== Default css ======-->
    <link rel="stylesheet" href="<?php echo css_url("default.css") ?>">
    
    <!--====== Style css ======-->
    <link rel="stylesheet" href="<?php echo css_url("style.css") ?>">
    
    <!--====== Responsive css ======-->
    <link rel="stylesheet" href="<?php echo css_url("responsive.css") ?>">
    
<script src="<?php echo js_url("app.js") ?>"></script>
  
  
</head>

<body>
   
    <!--====== PRELOADER PART START ======-->
    
    
    <!--====== PRELOADER PART START ======-->
    
    <!--====== HEADER PART START ======-->
    
    <header class="header-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" href="#">
                            <img src="assets/images/logo.png" alt="Logo">
                        </a> <!-- Logo -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="bar-icon"></span>
                            <span class="bar-icon"></span>
                            <span class="bar-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul id="nav" class="navbar-nav ml-auto">
                                <li class="nav-item active">
                                    <a  data-scroll-nav="0"  href="<?php echo base_url('',null).'welcome/index' ?>">Changer Caisse</a>
                                </li>
                                
                            </ul> <!-- navbar nav -->
                        </div>
                    </nav> <!-- navbar -->
                </div>
            </div> <!-- row -->
        </div> <!-- container -->
    </header>
    
    <!--====== HEADER PART ENDS ======-->
   
    <!--====== SLIDER PART START ======-->
    
    <section id="home" class="slider-area pt-150">
                <?php include($vue.".php"); ?> 
    </section>
   

    <a href="#" class="back-to-top"><i class="lni-chevron-up"></i></a>
    
    <!--====== BACK TO TOP PART ENDS ======-->
    
    
    
    
    
    
    
    
    
    
    <!--====== jquery js 
    <script src="<?php //echo js_url("vendor/modernizr-3.6.0.min.js"); ?>"></script>
    <script src="<?php //echo js_url("vendor/jquery-1.12.4.min.js"); ?>"></script>======-->

    <!--====== Bootstrap js 
    <script src="<?php //echo js_url("bootstrap.min.js"); ?>""></script>======-->
    
    
    <!--====== Slick js ======-->
    <script src="<?php echo js_url("slick.min.js"); ?>"></script>
    
    <!--====== Magnific Popup js ======-->
    <script src="<?php echo js_url("jquery.magnific-popup.min.js"); ?>"></script>

    
    <!--====== nav js ======-->
    <script src="<?php echo js_url("jquery.nav.js"); ?>"></script>
    
    <!--====== Nice Number js ======-->
    <script src="<?php echo js_url("jquery.nice-number.min.js"); ?>"></script>
    
    <!--====== Main js ======-->
    <script src="<?php echo js_url("main.js"); ?>"></script>

</body>

</html>
