<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->library('session');
		$this->session->sess_destroy();
		$data['vue']="changecaisse";
		$this->load->model('model1');
		$data['licaisse'] = $this->model1->get_caisse();
		$this->load->view('template',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	/*public function indexchoose()
	{
		$data['vue']="changecaisse";
		//$data['caisse'] = "(Choose Caisse)";
		$this->load->model('model1');
		$data['licaisse'] = $this->model1->get_caisse();
		$this->load->view('template',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}*/	

	public function indexcaisse()
	{
		$this->load->library('session');
		//$this->session->sess_destroy();
		$data['vue']='saisie';
		$this->load->model('model1');
		$this->session->set_userdata('caisse',$this->input->get_post("id"));
		$data['caisse'] = $this->session->userdata('caisse');
		//echo $this->session->userdata('caisse');
		$data['liprod'] = $this->model1->get_prod();
		$data['licate'] = $this->model1->get_cate();
		//$data['achat'] = $this->model1->get_achat_bycaisse($this->session->userdata('caisse'));
		//$data['prodachat'] = $this->model1->get_prod_achat($data['achat']);
		$this->load->view('template',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
		$this->load->helper('images_helper');
	}
	public function achat()
	{
		//echo "ato";
		$this->load->library('session');
		$data['vue']='saisie';
		$data['caisse'] = $this->session->userdata('caisse');
		$this->load->model('model1');
		$data['liprod'] = $this->model1->get_prod();
		$data['licate'] = $this->model1->get_cate();
		$prod = $this->input->get_post("code");
		$prd = $this->model1->get_prod_bycode($prod);
		$isa = $this->input->get_post("qtt");
		$listAchat = array();
		if ($this->session->userdata('listAchat') != null) $listAchat = $this->session->userdata('listAchat');
		$newprod['code'] = $prod;
		$newprod['qtt'] = $isa;
		$listAchat [] = $newprod;
		$this->session->set_userdata('listAchat',$listAchat);
		$data['listAchat'] = $this->session->userdata('listAchat');
		$this->load->view('template',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
		$this->load->helper('images_helper');

	}
	public function validerAchat()
	{
		$this->load->library('session');
		$data['vue']='saisie';
		$data['caisse'] = $this->session->userdata('caisse');
		$this->load->model('model1');
		$data['liprod'] = $this->model1->get_prod();
		$data['licate'] = $this->model1->get_cate();
		$listAchat = $this->session->userdata('listAchat');
		$this->model1->insertachat($data['caisse']);
		echo $data['caisse'];
		$lastAchat = $this->model1->getlastachat();
		//echo $lastAchat;
		for($i=0;$i<count($listAchat);$i++){
			$this->model1->insertdetachat($listAchat[$i], $lastAchat['lastId']);
			//echo $data['listAchat'][$i]['code'];
			//echo $data['listAchat'][$i]['qtt'];
		}
		$this->load->view('template',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
		$this->load->helper('images_helper');
		$this->session->set_userdata('listAchat',array());

	}

	public function listProd()
	{
		//$data = array();
		$this->load->library('session');
		$data['idCategorie'] = $_GET['idCategorie'];
		$this->load->model('model1');
		$data['liprodcate']=$this->model1->get_prod();
		//$data['liprod']=$this->model1->get_prod();
		//$data['vue']='saisie';
		$data['caisse'] = $this->session->userdata('caisse');
		$data['licate'] = $this->model1->get_cate();
		if($data['idCategorie'] != '') $data['liprodcate']=$this->model1->get_prodByCate($data['idCategorie']);
		$this->load->view('list',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
		$this->load->helper('images_helper');
		//$this->load->view('template',$data);
	}
	/*public function index2($txt)
	{
		$data['vue']=$txt;
		$this->load->view('template',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}	
	public function licaisse($vue)
	{
		$data = array();
		$data['vue']=$vue;
		$this->load->model('model1');
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
		$data['licaisse'] = $this->model1->get_caisse();
		$this->load->view('template',$data);
	}*/
}
?>