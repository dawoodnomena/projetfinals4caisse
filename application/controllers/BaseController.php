<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BaseController extends CI_Controller {
    public function __construct()
	{
		parent::__construct();
	}

	public function loginAdmin()
	{
		$data['vue']="loginAdmin";
		$this->load->model('model1');
		$data['licaisse'] = $this->model1->get_caisse();
		//$data['caisse'] = "(Choose Caisse)";
		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function pageCrud()
	{
		$data= array();
		$data['vue']="crud";
		$this->load->model('model1');
		$data['produit'] = $this->model1->get_prod();
		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function testLogin(){
		$data =array();
		$this->load->model('modelAdmin');
		$data['listeAdmin']= $this->modelAdmin->getCaissier();
		$data['test'] ="";
		for($i=0;$i<count($data['listeAdmin']);$i++){
			if($data['listeAdmin'][$i]['pseudo']==$this->input->get_post("nom")){
				if($data['listeAdmin'][$i]['mdp']==sha1($this->input->get_post("mdp"))){
					$data['vue']="accueilAdmin";
					$this->session->set_userdata('nom',$this->input->get_post("nom"));
					$data['nom'] = $this->session->userdata('nom');
				}
				else{
					$data['vue']="loginAdmin";
					$data['test']="mot de passe incorecte";
				}
			}
			else{
				$data['vue']="loginAdmin";
				$data['test']="pseudo incorecte";
			}
		}
		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function supprProd(){
		$data=array();
		$data['vue']="crud";
		$this->load->model('modelAdmin');
		$this->modelAdmin->suppProd($this->input->get_post("code"));
		$this->load->model('model1');
		$data['produit'] = $this->model1->get_prod();

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function pageModif(){
		$data=array();
		$data['vue']="modifier";
		$this->load->model('modelAdmin');
		$data['produit']=$this->modelAdmin->getProdByCode($this->input->get_post("code"));

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function modifProd(){
		$data=array();
		$this->load->model('modelAdmin');
		$this->modelAdmin->modifier($this->input->get_post("code"), $this->input->get_post("prix"), $this->input->get_post("quant"));

		$data['vue']="crud";
		$this->load->model('model1');
		$data['produit'] = $this->model1->get_prod();

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function pageInserer(){
		$data=array();
		$data['vue']="inserer";

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function insererProd(){
		$data= array();
		$data['vue']="crud";
		$this->load->model('modelAdmin');
		$data['idCat']=$this->modelAdmin->getIdCatByNom($this->input->get_post("categorie"));
		$code=$this->input->get_post("code");
		$idCat=$data['idCat'][0]['idCategorie'];
		$des= $this->input->get_post("designation");
		$prix=$this->input->get_post("prix");
		$quant=$this->input->get_post("quant");
		$this->modelAdmin->insertProd($code,$idCat ,$des, $prix,$quant);
		
		$this->load->model('model1');
		$data['produit'] = $this->model1->get_prod();

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function rechercher(){
		$data= array();
		$data['vue']="crud";
		$this->load->model('modelAdmin');
		$data['produit']=$this->modelAdmin->recherche($this->input->get_post("mot"));

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function pageCategorie(){
		$data= array();
		$data['vue']="categorie";
		$this->load->model('modelAdmin');
		$data['categorie']=$this->modelAdmin->getCategorie();

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');

	}

	public function supprCat(){
		$data=array();
		$data['vue']="categorie";
		$this->load->model('modelAdmin');
		$this->modelAdmin->supprCat($this->input->get_post("idCategorie"));
		$this->load->model('modelAdmin');
		$data['categorie'] = $this->modelAdmin->getCategorie();

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function pageCat(){
		$data=array();
		$data['vue']="modifierCat";
		$this->load->model('modelAdmin');
		$data['categorie']=$this->modelAdmin->getCatById($this->input->get_post("idCategorie"));

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function modifCat(){
		$data=array();
		$this->load->model('modelAdmin');
		$this->modelAdmin->modifierCat($this->input->get_post("idCategorie"), $this->input->get_post("nom"));

		$data['vue']="categorie";
		$this->load->model('model1');
		$data['categorie'] = $this->modelAdmin->getCategorie();

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function pageInsererCat(){
		$data=array();
		$data['vue']="insererCat";

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function insererCat(){
		$data= array();
		$data['vue']="categorie";
		$this->load->model('modelAdmin');
		$data['categorie'] = $this->modelAdmin->getCategorie();

		$id=$this->input->get_post("idCategorie");
		$nom= $this->input->get_post("nom");
		$this->modelAdmin->insertCat($id,$nom );
	}

	public function rechercherCat(){
		$data= array();
		$data['vue']="categorie";
		$this->load->model('modelAdmin');
		$data['categorie']=$this->modelAdmin->rechercheCat($this->input->get_post("mot"));

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');
	}

	public function deconnex(){
		$this->session->sess_destroy();
		$data['vue']="loginAdmin";

		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');

	}

	public function statistique(){
		$data['vue'] = "listStat";
		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');

	}

	public function achatSem(){
		$data['vue'] = "achatSem";
		$this->load->model('modelAdmin');
		$data['lachatSem'] = $this->modelAdmin->getAchatSem();
		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');

	}

	public function statProd(){
		$data['vue'] = "statProd";
		$this->load->model('modelAdmin');
		$data['lstatProd'] = $this->modelAdmin->getStatProd();
		$this->load->view('templateAdmin',$data);
		$this->load->helper('css_helper');
		$this->load->helper('vendor_helper');

	}
}
?>